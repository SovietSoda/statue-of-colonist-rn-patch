﻿using System.Collections.Generic;
using HarmonyLib;
using RimWorld;
using Verse;
using StatueOfColonist;
using UnityEngine;

namespace socrnw
{
	[HarmonyPatch(typeof(StatueOfColonistRenderer))]
	[HarmonyPatch("DrawAddons")]
	internal class socrnw_StatueOfColonistRenderer_DrawAddons_Patch
	{
		private static void Postfix(StatueOfColonistRenderer __instance, StatueOfColonistGraphicSet graphics, bool portrait, Vector3 vector, Vector3 headOffset, ThingDef raceDef, Quaternion quat, Rot4 rotation, BodyTypeDef bodyTypeDef, float scale)
		{
			// redraw hair at a higher y value to be over boobs

			// this function is basically a copy of the hair-relevant stuff from the StatueOfColonistRenderer.Render function of the StatueOfColonist mod.
			// as it postfixes the DrawAddons() function some parameters aren't available and have to be read in other ways
			Building_StatueOfColonist.HeadRenderMode? headRenderMode = Traverse.Create(__instance.parent).Field("headRenderMode").GetValue() as Building_StatueOfColonist.HeadRenderMode?;
            if (headRenderMode == null)
            {
				headRenderMode = Building_StatueOfColonist.HeadRenderMode.HairOnly;
			}

			if (graphics.headGraphic != null)
			{
				Vector3 vector3 = quat * __instance.BaseHeadOffsetAt(rotation, graphics.data.bodyType, raceDef, __instance.parent.Data.lifeStageDef);
				vector3 = new Vector3(vector3.x * scale, vector3.y, vector3.z * scale);

				//Vector3 vector4 = rootLoc + vector3; // rootloc not available here, but it's just a little less y than vector2 (i.e. vector)
				Vector3 vector4 = vector + vector3; // so by using "vector" the hair will automatically be drawn over boobs
				vector4.y += 0.03125f;
				bool flag = false;
				if (!portrait || !Prefs.HatsOnlyOnMap)
				{

					// unfortunately, redrawing hair higher also means redrawing hats higher -.-
					Mesh hairMesh = __instance.GetHairMesh(graphics, portrait, raceDef, rotation);
					List<ApparelGraphicRecord> apparelGraphics = graphics.apparelGraphics;
					for (int j = 0; j < apparelGraphics.Count; j++)
					{
						if (apparelGraphics[j].sourceApparel.def.apparel.LastLayer == ApparelLayerDefOf.Overhead && headRenderMode != Building_StatueOfColonist.HeadRenderMode.HairOnly)
						{
							if (headRenderMode == Building_StatueOfColonist.HeadRenderMode.Default)
							{
								flag = true;
							}
							Material mat = apparelGraphics[j].graphic.MatAt(rotation, null);
							Vector3 loc2 = vector4;
							loc2.y += 0.001f * (float)(j + 1) * scale;
							GenDraw.DrawMeshNowOrLater(hairMesh, loc2, quat, mat, portrait);
						}
					}
				}
				if (!flag && graphics != null)
				{
					// finally hair here?
					Mesh hairMesh4 = __instance.GetHairMesh(graphics, portrait, raceDef, rotation);
					Material material3 = graphics.HairMatAt(rotation);
					if (hairMesh4 != null && material3 != null)
					{
						if (socrnw.DebugMode && GenTicks.TicksGame % 1000 == 0)
							Log.Message("############ redrawing hair " + material3.name + " at (" + vector4.x + ", " + vector4.y + ", " + vector4.z + ")");
						GenDraw.DrawMeshNowOrLater(hairMesh4, vector4, quat, material3, portrait);
					}
				}
			}

		}
	}
}