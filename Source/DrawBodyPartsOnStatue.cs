﻿using System;
using System.Collections.Generic;
using System.Linq;
using HarmonyLib;
using Verse;
using StatueOfColonist;
using AlienRace;
using UnityEngine;
using RimWorld;

namespace socrnw
{
	[HarmonyPatch(typeof(StatueOfColonistGraphicSet))]
	[HarmonyPatch("ResolveAllGraphics")]
	internal static class socrnw_StatueOfColonistGraphicSet_ResolveAllGraphics_Patch
	{
		private static bool Prefix(StatueOfColonistGraphicSet __instance, float scale)
		{
			// only run if enabled, otherwise original already did all the things
			if (socrnw.OverrideResolveGraphics)
			{
				return false;
			}
			return true;
		}

		// better resolve graphics that includes hediff info like HAR does, and populates graphics array for humans
		private static void Postfix(StatueOfColonistGraphicSet __instance, float scale)
		{
			if (socrnw.DebugMode)
				Log.Message("############################################## socrnw_StatueOfColonistGraphicSet_ResolveAllGraphics_Patch");

			StatueOfColonistData data = __instance.data;
			if (data == null)
			{
				return;
			}
			ThingDef_AlienRace raceDef_AlienRace = data.raceDef as ThingDef_AlienRace;
			
			if (raceDef_AlienRace == null)
			{
				return;
			}
			/*ThingDef raceDef = data.raceDef;
			if (raceDef == null)
			{
				return;
			}*/
			string a = raceDef_AlienRace.defName;
			if (a == null)
			{
				return;
			}

			Building_StatueOfColonist parent = __instance.parent;
			AlienPartGenerator.AlienComp alienComp = (parent != null) ? parent.GetComp<AlienPartGenerator.AlienComp>() : null;
			GraphicPaths currentGraphicPath = raceDef_AlienRace.alienRace.graphicPaths.GetCurrentGraphicPath(__instance.data.lifeStageDef);
			alienComp.customDrawSize = currentGraphicPath.customDrawSize;
			alienComp.customPortraitDrawSize = currentGraphicPath.customPortraitDrawSize;
			Traverse.Create(alienComp).Method("AssignProperMeshs", Array.Empty<object>()).GetValue();
			Shader shader = ShaderDatabase.LoadShader(__instance.data.shaderCutoutPath);

			// socrnw.OverrideResolveGraphics prevents original function from running, thus it's neccessary to do everything here
			if (socrnw.OverrideResolveGraphics) // non-humans are handled in prefix in soc har patch, humans in original soc
			{
				__instance.data.headGraphicPath = AlienPartGenerator.GetAlienHead(currentGraphicPath.head, raceDef_AlienRace.alienRace.generalSettings.alienPartGenerator.useGenderedHeads ? __instance.data.gender.ToString() : "", alienComp.crownType);
				__instance.nakedGraphic = raceDef_AlienRace.alienRace.generalSettings.alienPartGenerator.GetNakedGraphic(__instance.data.bodyType, ShaderDatabase.CutoutSkin, __instance.data.color, __instance.data.color, currentGraphicPath.body, __instance.data.gender.ToString());
				__instance.rottingGraphic = raceDef_AlienRace.alienRace.generalSettings.alienPartGenerator.GetNakedGraphic(__instance.data.bodyType, ShaderDatabase.CutoutSkin, PawnGraphicSet.RottingColorDefault, PawnGraphicSet.RottingColorDefault, currentGraphicPath.body, __instance.data.gender.ToString());
				__instance.dessicatedGraphic = GraphicDatabase.Get<Graphic_Multi>((currentGraphicPath.skeleton == "Things/Pawn/Humanlike/HumanoidDessicated") ? __instance.data.bodyType.bodyDessicatedGraphicPath : currentGraphicPath.skeleton, ShaderDatabase.Cutout);
				__instance.headGraphic = GraphicDatabase.Get<Graphic_Multi>(__instance.data.headGraphicPath, ShaderDatabase.CutoutSkin, Vector2.one, __instance.data.color);
				__instance.desiccatedHeadGraphic = GraphicDatabase.Get<Graphic_Multi>(__instance.data.headGraphicPath, ShaderDatabase.Cutout, Vector2.one, __instance.data.color);
				__instance.skullGraphic = GraphicDatabase.Get<Graphic_Multi>(currentGraphicPath.skull, ShaderDatabase.Cutout, Vector2.one, __instance.data.color);
				if (!__instance.data.hairGraphicPath.NullOrEmpty())
				{
					__instance.hairGraphic = GraphicDatabase.Get<Graphic_Multi>(__instance.data.hairGraphicPath, shader, Vector2.one, __instance.data.color);
				}
				__instance.headStumpGraphic = GraphicDatabase.Get<Graphic_Multi>(currentGraphicPath.stump, ShaderDatabase.Cutout, Vector2.one, __instance.data.color);
				__instance.desiccatedHeadStumpGraphic = GraphicDatabase.Get<Graphic_Multi>(currentGraphicPath.stump, ShaderDatabase.Cutout, Vector2.one, PawnGraphicSet.RottingColorDefault);
			}

			// this part has to be done for all the races as it respects hediffs and graphics list for humans is neither populated by original soc nor the soc har patch
			AlienPartGenerator alienPartGenerator = raceDef_AlienRace.alienRace.generalSettings.alienPartGenerator;

			__instance.bodyAddonGraphics = new List<Graphic>();
			if (alienComp.addonVariants == null)
			{
				alienComp.addonVariants = new List<int>();
			}
			int item = 0;
			for (int i = 0; i < alienPartGenerator.bodyAddons.Count; i++)
			{
				Graphic path = socrnw_StatueOfColonistGraphicSet_ResolveAllGraphics_Patch.GetPathWithHediffs(alienPartGenerator.bodyAddons[i], __instance.data, parent, ref item, (alienComp.addonVariants.Count > i) ? new int?(alienComp.addonVariants[i]) : null, shader);
				__instance.bodyAddonGraphics.Add(path);
				if (alienComp.addonVariants.Count <= i)
				{
					alienComp.addonVariants.Add(item);
				}
			}


			if (socrnw.OverrideResolveGraphics)
			{
				__instance.ResolveTatooGraphic();
				__instance.ResolveBoardGraphic();
				__instance.ResolveApparelGraphics();
				__instance.forceResolve = false;
			}
		}

		public static Graphic GetPath(AlienPartGenerator.BodyAddon bodyAddon, StatueOfColonistData data, Building_StatueOfColonist statue, ref int sharedIndex, int? savedIndex = null, Shader shader = null)
		{
			// this is more or less the original GetPath from the SoC HAR patch
			string path = bodyAddon.path;
			int variantCount = bodyAddon.variantCount;
			if (shader == null)
			{
				if (ContentFinder<Texture2D>.Get(bodyAddon.path + "_northm", false) == null)
				{
					shader = bodyAddon.ShaderType.Shader;
				}
				else
				{
					shader = ShaderDatabase.CutoutComplex;
				}
			}
			if (path.NullOrEmpty())
			{
				return null;
			}
            if (variantCount <= 0)
            {
				variantCount = 1;
            }
			int num = (savedIndex != null) ? (sharedIndex = savedIndex.Value) : (bodyAddon.linkVariantIndexWithPrevious ? (sharedIndex % variantCount) : (sharedIndex = Rand.Range(0, variantCount)));

			Graphic g = null;

			if (path.Contains("FeaturelessCrotch") && socrnw.HideMissingFeaturelessCrotchException)
				num=0;

			g = GraphicDatabase.Get<Graphic_Multi>(path + ((num == 0) ? "" : num.ToString()), shader, bodyAddon.drawSize * 1.5f, data.color);

			return g;
		}

		public static Graphic GetPathWithHediffs(AlienPartGenerator.BodyAddon bodyAddon, StatueOfColonistData data, Building_StatueOfColonist statue, ref int sharedIndex, int? savedIndex = null, Shader shader = null)
        {
			// this is the hediff-respecting part from HAR (AlienPartGenerator.BodyAddon.GetPath)
			// TODO: also include backstory?
			if (socrnw.DebugMode)
				Log.Message("############################################## socrnw GetPathWithHediffs");

			List<HediffInfo> hs = null;
			StatuePartsComp spc = null;
			if (statue != null)
            {
				spc = statue.GetComp<StatuePartsComp>();
				hs = spc?.StatueHediffInfo;
            }

			string returnPath = string.Empty;
			int variantCounting = 0;

			if (hs != null && !bodyAddon.hediffGraphics.NullOrEmpty())
			{
				foreach (AlienPartGenerator.BodyAddonHediffGraphic bahg in bodyAddon.hediffGraphics)
				{
					foreach (HediffInfo h in hs.Where(predicate: h => h.def == bahg.hediff &&
																				(h.PartIsNull ||
																				bodyAddon.bodyPart.NullOrEmpty() ||
																				h.BodyPartUntranslatedCustomLabel == bodyAddon.bodyPart ||
																				h.BodyPartDefName == bodyAddon.bodyPart)))
					{
						returnPath = bahg.path;
						variantCounting = bahg.variantCount;

						if (!bahg.severity.NullOrEmpty())
							foreach (AlienPartGenerator.BodyAddonHediffSeverityGraphic bahsg in bahg.severity)
							{
								if (h.Severity >= bahsg.severity)
								{
									returnPath = bahsg.path;
									variantCounting = bahsg.variantCount;
									break;
								}
							}
						break;
					}
				}
			}

			if (returnPath.NullOrEmpty())
			{
				return GetPath(bodyAddon, data, statue, ref sharedIndex, savedIndex, shader);
			}

			if(socrnw.DebugMode)
				Log.Message("############################################## socrnw Hediff path found? " + returnPath);

			// if statue is set to not be erect, try to get flaccid penis texture
			bool flaccid = (spc?.erect == false);
			if (flaccid && returnPath.Length >= 9 && returnPath.Contains("penis"))
			{
				string modifiedPath = returnPath.Insert(9, "Flaccid/");

				if (ContentFinder<Texture2D>.Get(modifiedPath + "_north", false) != null)
				{
					returnPath = modifiedPath;
				}
			}

			if (socrnw.usingOTY)
            {
				bool validTexture = false;

				if (data.bodyType == BodyTypeDefOf.Hulk || data.bodyType == BodyTypeDefOf.Fat)
				{
					if (data.bodyType == BodyTypeDefOf.Hulk)
					{
						if ((ContentFinder<Texture2D>.Get(returnPath + "_Hulk" + "_south", false) != null))
						{
							returnPath = returnPath + "_Hulk";
							validTexture = true;
						}
					}
					else if (data.bodyType == BodyTypeDefOf.Fat)
					{
						if ((ContentFinder<Texture2D>.Get(returnPath + "_Fat" + "_south", false) != null))
						{
							returnPath = returnPath + "_Fat";
							validTexture = true;
						}
					}
					if (validTexture == false)
					{
						if ((ContentFinder<Texture2D>.Get(returnPath + "_Wide" + "_south", false) != null))
						{
							returnPath = returnPath + "_Wide";
							validTexture = true;
						}
					}
				}
				else if (data.bodyType == BodyTypeDefOf.Thin)
				{
					if ((ContentFinder<Texture2D>.Get(returnPath + "_Thin" + "_south", false) != null))
					{
						returnPath = returnPath + "_Thin";
						validTexture = true;
					}
				}
				else if (data.bodyType == BodyTypeDefOf.Male)
				{
					if ((ContentFinder<Texture2D>.Get(returnPath + "_Male" + "_south", false) != null))
					{
						returnPath = returnPath + "_Male";
						validTexture = true;
					}
				}
				else if (data.bodyType == BodyTypeDefOf.Female)
				{
					if ((ContentFinder<Texture2D>.Get(returnPath + "_Female" + "_south", false) != null))
					{
						returnPath = returnPath + "_Female";
						validTexture = true;
					}
				}
				else
				{
					string bodyname = data.bodyType.defName;
					if ((ContentFinder<Texture2D>.Get(returnPath + "_" + bodyname + "_south", false) != null))
					{
						returnPath = returnPath + "_"+ bodyname;
						validTexture = true;
					}
				}
			}

			if (socrnw.DebugMode)
				Log.Message("############################################## socrnw Hediff path found (2)? " + returnPath);

			if (variantCounting <= 0)
				variantCounting = 1;

			int num = savedIndex.HasValue ? (sharedIndex = savedIndex.Value % variantCounting)
										  : (bodyAddon.linkVariantIndexWithPrevious ? sharedIndex % variantCounting
																			   : (sharedIndex = Rand.Range(min: 0, variantCounting)));

			Graphic g=null;

			g = GraphicDatabase.Get<Graphic_Multi_RotationFromData>(returnPath += (num == 0) ? "" : num.ToString(),
															shader,
															bodyAddon.drawSize * 1.5f,
															data.color);

			return g;


		}
	}
}
