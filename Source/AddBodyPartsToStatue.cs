﻿using System;
using System.Collections.Generic;
using HarmonyLib;
using UnityEngine;
using Verse;
using StatueOfColonist;

namespace socrnw
{
	public struct HediffInfo : IExposable
	{
		// just the info that is needed to draw them appropriately
		public HediffDef def;
		public float Severity;
		public bool PartIsNull;
		public String BodyPartDefName;
		public String BodyPartUntranslatedCustomLabel;

		public void ExposeData()
        {
			Scribe_Defs.Look<HediffDef>(ref def, "def");
			Scribe_Values.Look<float>(ref Severity, "Severity");
			Scribe_Values.Look<bool>(ref PartIsNull, "PartIsNull");
			Scribe_Values.Look<String>(ref BodyPartDefName, "BodyPartDefName");
			Scribe_Values.Look<String>(ref BodyPartUntranslatedCustomLabel, "BodyPartUntranslatedCustomLabel");
		}
	}

	public class StatuePartsComp : ThingComp
	{
		// a comp used to attach (and save) necessary hediff info to a statue
		public List<HediffInfo> StatueHediffInfo;

		public bool erect;

		public StatuePartsComp()
        {
			
		}

		public override void PostExposeData()
        {
			base.PostExposeData();

			if (socrnw.DebugMode)
				Log.Message("############################################## socrnw PostExposeData()");

			Scribe_Collections.Look<HediffInfo>(ref StatueHediffInfo, "statueHediffs", LookMode.Deep);
			Scribe_Values.Look<bool>(ref erect, "erect", true);
		}
    }

	[HarmonyPatch(typeof(Building_StatueOfColonist))]
	[HarmonyPatch("PreResolveGraphicsFromPawn")]
	[HarmonyPatch(new Type[]
	{
		typeof(Pawn)
	})]
	internal class socrnw_Building_StatueOfColonist_PreResolveGraphicsFromPawn_Patch
	{
		private static void Prefix(Building_StatueOfColonist __instance, Pawn pawn)
		{
			if (socrnw.DebugMode)
				Log.Message("############################################## socrnw_Building_StatueOfColonist_PreResolveGraphicsFromPawn_Patch");
			
			// copy info from hediffs
			List<HediffInfo> L = new List<HediffInfo>();
			foreach (Hediff h in pawn.health.hediffSet.hediffs)
			{
                HediffInfo x = new HediffInfo
                {
                    def = h.def,
                    Severity = h.Severity
                };
                if (h.Part == null)
				{
					x.PartIsNull = true;
					x.BodyPartDefName = null;
					x.BodyPartUntranslatedCustomLabel = null;
				}
				else
				{
					x.PartIsNull = false;
					x.BodyPartDefName = h.Part.def.defName;
					x.BodyPartUntranslatedCustomLabel = h.Part.untranslatedCustomLabel;
				}
				L.Add(x);
			}
			__instance.GetComp<StatuePartsComp>().StatueHediffInfo = L;
		}
	}

	// this would copy stuff to the preset, but there are no fields in the preset to save hediffs
	/*[HarmonyPatch(typeof(StatueOfColonistPreset))]
	[HarmonyPatch("SetStatueOfColonist")]
	internal class SOCRNW_StatueOfColonistPreset_SetStatueOfColonist_Patch
	{
		private static void Postfix(StatueOfColonistPreset __instance, Building_StatueOfColonist statue)
		{
			AlienPartGenerator.AlienComp comp = statue.GetComp<AlienPartGenerator.AlienComp>();
			if (comp != null)
			{
				__instance.alienCrownType = comp.crownType;
				__instance.addonVariants = comp.addonVariants;
			}
		}
	}//*/

	// this would copy stuff from the preset, but the preset doesn't contain Hediff info
	/*[HarmonyPatch(typeof(Building_StatueOfColonist))]
	[HarmonyPatch("CopyStatueOfColonistFromPreset")]
	internal class Building_StatueOfColonist_CopyStatueOfColonistFromPreset_Patch
	{
		private static void Postfix(Building_StatueOfColonist __instance, StatueOfColonistPreset preset)
		{
			if (preset.raceDefName != "Human")
			{
				__instance.GetComp<AlienPartGenerator.AlienComp>().crownType = preset.alienCrownType;
				__instance.GetComp<AlienPartGenerator.AlienComp>().addonVariants = preset.addonVariants;
			}
		}
	}//*/

	[HarmonyPatch(typeof(Window_EditStatue))]
	[HarmonyPatch("CopyFromColonistInternal")]
	[HarmonyPatch(new Type[]
	{
		typeof(Pawn)
	})]
	internal static class socrnw_Window_EditStatue_CopyFromColonistInternal_Patch
	{
		private static void Postfix(Window_EditStatue __instance, Pawn p)
		{
			if(socrnw.DebugMode)
				Log.Message("############################################## socrnw_Window_EditStatue_CopyFromColonistInternal_Patch");

			// copy info from hediffs
			List<HediffInfo> L = new List<HediffInfo>();
			foreach (Hediff h in p.health.hediffSet.hediffs)
			{
                HediffInfo x = new HediffInfo
                {
                    def = h.def,
                    Severity = h.Severity
                };
                if (h.Part == null)
				{
					x.PartIsNull = true;
					x.BodyPartDefName = null;
					x.BodyPartUntranslatedCustomLabel = null;
				} else {
					x.PartIsNull = false;
					x.BodyPartDefName = h.Part.def.defName;
					x.BodyPartUntranslatedCustomLabel = h.Part.untranslatedCustomLabel;
				}
				L.Add(x);
			}
			__instance.Statue.GetComp<StatuePartsComp>().StatueHediffInfo = L;

		}
	}
	
	[StaticConstructorOnStartup]
	internal class socrnw_DefGenerator_GenerateImpliedDefs_PreResolve_Patch
	{
		static socrnw_DefGenerator_GenerateImpliedDefs_PreResolve_Patch()
		{
			LongEventHandler.ExecuteWhenFinished(delegate
			{
				foreach (ThingDef thingDef in DefDatabase<ThingDef>.AllDefs)
				{
					if (thingDef != null && (thingDef.thingClass == typeof(Building_StatueOfColonist) || thingDef.defName == "TMB_MinifiedStatueOfColonistK" || thingDef.defName.Contains("TMB_StatueOfColonistK")))
					{
						// add my own comp to the statue, so I can save stuff in it
						thingDef.comps.Add(new CompProperties(typeof(StatuePartsComp)));
					}
				}
			});
		}
	}

	// add a toggle to, well, toggle the erection of the statue
	// TODO: only show if statue has a penis
	[HarmonyPatch(typeof(Building_StatueOfColonist))]
	[HarmonyPatch("GetStatueGizmos")]
	internal static class socrnw_AddGizmoToStatue_Patch
	{
		// add toggle for erection
		private static void Postfix(Building_StatueOfColonist __instance, ref IEnumerable<Gizmo> __result)
		{
			__result = Process(__instance, __result);
		}

		public static IEnumerable<Gizmo> Process(Building_StatueOfColonist __instance, IEnumerable<Gizmo> __result)
        {
			Command_Toggle command_Toggle = new Command_Toggle
			{
				defaultDesc = "Toggle Erection",
				icon = ContentFinder<Texture2D>.Get("UI/Commands/toggle_erection"),
				//ContentFinder<Texture2D>.Get("UI/Buttons/Rename", true),
				defaultLabel = "Erect",
				isActive = ()=>__instance.GetComp<StatuePartsComp>().erect,
				toggleAction = (()=> { __instance.GetComp<StatuePartsComp>().erect = !__instance.GetComp<StatuePartsComp>().erect;
					__instance.ResolveGraphics();
					__instance.ForceResolveWhenRendering();
				})
			};
			yield return command_Toggle;

			foreach (var gizmo in __result)
			{
				yield return gizmo;
			}
		}
	}
}
