﻿using HugsLib;
using HugsLib.Settings;
using HarmonyLib;
using Verse;

namespace socrnw
{
    public class socrnw : ModBase
    {
        public override string ModIdentifier => "socrnw";

        public static bool usingOTY;

        public static SettingHandle<bool> DebugMode;
        public static SettingHandle<bool> HideMissingFeaturelessCrotchException;
        public static SettingHandle<bool> OverrideResolveGraphics;

        public override void DefsLoaded()
        {
            usingOTY = (LoadedModManager.RunningModsListForReading.Any(x => x.PackageId == "OTYOTY.NudePatchForRimNudeWorld.UnofficialUpdate"));
            if (!usingOTY)
                usingOTY = (LoadedModManager.RunningModsListForReading.Any(x => x.PackageId == "OTYOTY.NudePatchForRimNudeWorld"));
            if (!usingOTY)
            {
                if (HediffDef.Named("RNW_BellyBulge") != null)
                    usingOTY = true;
            }

            /*if (usingOTY) Log.Message("############################################## socrnw - found OTY");
            else Log.Message("############################################## socrnw - did NOT find OTY");//*/

            DebugMode = Settings.GetHandle("DebugMode", "DebugMode", "Log Output, mostly", false);
            HideMissingFeaturelessCrotchException = Settings.GetHandle("HideMissingFeaturelessCrotchException", "HideMissingFeaturelessCrotchException", "Workaround for \"Failed to find any textures at Genitals/FeaturelessCrotch2\"", true);
            OverrideResolveGraphics = Settings.GetHandle("OverrideResolveGraphics", "OverrideResolveGraphics", "Might not be necessary, but might prevent some things from happening twice.", true);
        }
    }
}
